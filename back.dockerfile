FROM node:13.14.0-buster-slim AS files

RUN apt-get update && \
    apt-get install -y ssh git

RUN mkdir -p ~/.ssh && \
    mkdir -p /root/build
RUN touch /root/.ssh/known_hosts && \
    ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts
COPY keys/id_rsa /root/.ssh/

RUN chmod 0600 /root/.ssh/id_rsa

WORKDIR /root/build
RUN git clone git@bitbucket.org:ppysiaki/pp-backend.git /root/build
COPY backend/application.properties /root/build/src/main/resources/

FROM maven:3.6.3-openjdk-14-slim AS mvn
COPY --from=files /root/build ./root/build
RUN ls -alX && pwd
RUN mvn -f /root/build/pom.xml compile war:war

FROM tomcat:9.0.31-jdk13-openjdk-oracle
COPY --from=mvn /root/build/target/pp-app-pp.war .
RUN ls -alX
RUN mv pp-app-pp.war $CATALINA_HOME/webapps/ROOT.war

WORKDIR $CATALINA_HOME
CMD ["catalina.sh", "run"]