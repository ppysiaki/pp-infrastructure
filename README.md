# Infrastructure

Website will be available at http://127.0.0.1:9000. (host pp-inf-front in container)   
API is available at 127.0.0.1:8080 (host pp-inf-back in container)  
DB is available at 127.0.0.1:3306 (host pp-inf-db in container) 

## Docker compose

Run
`docker-compose up -d`

### Execute tests

`docker-compose exec -it pp-inf-front npm test`

## Bare Docker

1. `docker network create --driver bridge pp-net`
2. `docker volume create pp-sql`
3. `docker run --name pp-inf-front --rm --network pp-net -p9000:9000 infrastructure_pp-inf-front`
4. `docker run --name pp-inf-sql --rm --network pp-net -p3306:3306 -v pp-sql:/var/lib/mysql infrastructure_pp-inf-sql --default-authentication-plugin=mysql_native_password --init-file=/init.sql`
5. Wait for database to initialize.
6. `docker run --name pp-inf-back --rm -p8080:8080 --network pp-net infrastructure_pp-inf-back`

### Execute tests

`docker exec -it pp-inf-front npm test`

## Swagger

Backend exposes Swagger at URL: http://127.0.0.1:8080/swagger-ui.html