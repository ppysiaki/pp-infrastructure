FROM node:13.14.0-buster-slim

RUN apt-get update && \
    apt-get install --no-install-recommends -y openssh-client libgtk2.0-0 libgtk-3-0 libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb git && \
    rm -rf /var/lib/apt/lists/* && \
    mkdir -p ~/.ssh && \
    mkdir -p /root/app && \
    touch /root/.ssh/known_hosts && \
    ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

COPY keys/id_rsa /root/.ssh/
RUN chmod 0600 /root/.ssh/id_rsa

WORKDIR /root/app
RUN git clone git@bitbucket.org:ppysiaki/pp-frontend.git /root/app && \
    npm install

CMD ["npm", "run", "start-docker"]